CREATE TABLE events( 
  id int auto_increment primary key, 
  name varchar(255), 
  properties json, 
  browser json
);