# Let's set some reasonable defaults
MYSQL_USER ?= root
MYSQL_PW ?= tutorial
MYSQL_HOST ?= 127.0.0.1
MYSQL_DATABASE ?= tutorial
MYSQL_PORT ?= 3310

# This DNS is used throughout the file.
MYSQL_DNS ?= $(MYSQL_USER):$(MYSQL_PW)@tcp($(MYSQL_HOST):$(MYSQL_PORT))/$(MYSQL_DATABASE)

# Number of migrations - this is optionally used on up and down commands
N?=

dns:
	@ echo '$(MYSQL_DNS)'

# DB tasks
clean-db: 
	docker stop db && docker rm db && docker volume rm mysql-with-json_db-data

migrate-setup:
	@ if [ -z "$$(which migrate)" ]; then echo "Installing migrate command..."; go get -tags mysql github.com/golang-migrate/migrate/v4/cmd/migrate; fi

migrate-up: migrate-setup
	@ echo "Running migrations..."
	@ migrate -database 'mysql://$(MYSQL_DNS)?multiStatements=true' -path migrations up $(N)

migrate-down: migrate-setup
	@ migrate -database 'mysql://$(MYSQL_DNS)?multiStatements=true' -path migrations down $(N)

migration-version: migrate-setup
	@ migrate -database 'mysql://$(MYSQL_DNS)?multiStatements=true' -path migrations version

migration-force-version: migrate-setup
	@ if [ -z "$(VERSION)" ]; then echo >&2 please set the the desired version via the variable VERSION; exit 2; fi
	@ migrate -database 'mysql://$(MYSQL_DNS)?multiStatements=true' -path migrations force $(VERSION)

create-migration: migrate-setup
	@ if [ -z "$(NAME)" ]; then echo >&2 please set the name of the migration via the variable NAME; exit 2; fi
	@ migrate create -ext sql -dir migrations -seq -digits 4 $(NAME) && ls -1r migrations/ | grep $(NAME) | head -2

go-fmt:
	@ go fmt ./...

local-db:
	@ echo "Setting up local MySQL..."

	@ docker-compose up -d

	@ until mysql --host=$(MYSQL_HOST) --port=$(MYSQL_PORT) --user=$(MYSQL_USER) -p$(MYSQL_PW) --protocol=tcp -e 'SELECT 1' >/dev/null 2>&1 && exit 0; do \
	  >&2 echo "MySQL not ready, sleeping for 5 secs..."; \
	  sleep 5 ; \
	done

	@ echo "... MySQL is up and running!"

build-local:
	@ go build main.go

run: go-fmt build-local local-db migrate-up
	@./main
