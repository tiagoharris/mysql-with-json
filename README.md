## mysql-with-json

Sample project to show how to handle JSON fields in a MySQL v5.7 table in Golang.

## Running it

```
make run
```